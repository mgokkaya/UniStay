"""UniStay URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from app import views
from django.conf import settings
from django.conf.urls.static import static
urlpatterns = [
    
    url(r'^admin/', admin.site.urls),
    url(r'^$',views.index),#tamam
    url(r'^ilan_olustur/$',views.Form),#tamam
    url(r'^detay/(?P<emlak_id>[0-9]+)/$',views.detay),#tamam
    url(r'^register/$',views.register),
    url(r'^profil/$',views.profil),#tamam
    url(r'^ilanlar/$',views.tum_ilanlar),#tamam
    url(r'^profilim/(?P<user_id>[0-9]+)/$',views.profilim),#tamam acikla:insanlar tarafindan gozuken
    url(r'^giris/$',views.sign_in),
    url(r'^auth/$',views.auth_view),
    url(r'^logout/$',views.cikis),
    url(r'^ara/$', views.Search, name="ara"),
    url(r'^tum_ilanlar/$', views.butun_ilanlar),
    url(r'^profil_register/$', views.profil_register),
    url(r'^sil/(?P<emlak_id>[0-9]+)/$',views.ilan_sil),#tamam
    url(r'^ilan_guncelle/(?P<ilan_id>[0-9]+)/$',views.ilan_guncelle),
    url(r'^ajente_listesi/$', views.ajente_listesi),


]   +  static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)