# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2018-08-10 11:52
from __future__ import unicode_literals

import app.models
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Banyo_tipi',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('banyo_tipi_ad', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='Cinsiyet',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('cinsiyet_ad', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='Dokumanlar',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('image_url', models.ImageField(default='image/kapak/none.jpg', upload_to=app.models.image_address2)),
            ],
        ),
        migrations.CreateModel(
            name='Emlak_bilgileri',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('ilan_detayii', models.TextField(default=None, max_length=5000)),
                ('ilan_baslik', models.CharField(max_length=250)),
                ('adres1', models.CharField(max_length=250)),
                ('adres2', models.CharField(default=None, max_length=250)),
                ('ilce', models.CharField(max_length=100)),
                ('sehir', models.CharField(max_length=100)),
                ('posta_kodu', models.CharField(default=None, max_length=20)),
                ('ilan_tarihi', models.DateTimeField()),
                ('website', models.CharField(default=None, max_length=100)),
                ('oda_sayisi', models.IntegerField()),
                ('mutfak_sayisi', models.IntegerField()),
                ('banyo_sayisi', models.IntegerField()),
                ('salon_sayisi', models.IntegerField()),
                ('yatak_oda_sayi', models.IntegerField()),
                ('yatak_sayisi', models.IntegerField()),
                ('genislik', models.IntegerField()),
                ('baslangic_tarihi', models.DateTimeField(default=django.utils.timezone.now)),
                ('aylik_ucret', models.FloatField()),
                ('depozito', models.FloatField()),
                ('minumum_kalis', models.IntegerField()),
                ('banyo_tipi', models.ManyToManyField(to='app.Banyo_tipi')),
                ('cinsiyet', models.ManyToManyField(to='app.Cinsiyet')),
            ],
        ),
        migrations.CreateModel(
            name='Ev_kolayliklari',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('ev_kolayliklari_ad', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='Galeri',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('galeri_adi', models.CharField(max_length=250)),
                ('kapak_image', models.ImageField(default='image/kapak/none.jpg', upload_to='image/kapak/')),
                ('e_bilgileri', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app.Emlak_bilgileri')),
            ],
        ),
        migrations.CreateModel(
            name='Konut_Tipi',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('konut_tipi_ad', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='Lokasyonlar',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('lok_ad', models.CharField(max_length=100)),
                ('lok_x', models.FloatField()),
                ('lok_y', models.FloatField()),
            ],
        ),
        migrations.CreateModel(
            name='Mulk_kurallari',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('mulk_kurallari_ad', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='Oda_Tipi',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('oda_tipi_ad', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='Profil_info',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('image_url', models.ImageField(default='documents/no_image.png', upload_to=app.models.image_address)),
                ('telefon', models.CharField(max_length=12)),
                ('meslek', models.CharField(max_length=250)),
                ('okul', models.CharField(max_length=250)),
                ('dogum_tarihi', models.DateTimeField(default='1980-01-01')),
                ('biyografi', models.TextField(default=None, max_length=5000)),
                ('facebook', models.CharField(default=None, max_length=500)),
                ('twitter', models.CharField(default=None, max_length=500)),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Profil_ranked',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('sayac', models.IntegerField(default=0)),
                ('toplam', models.IntegerField(default=0)),
                ('puan', models.IntegerField(default=0)),
                ('profil', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='app.Profil_info')),
            ],
        ),
        migrations.CreateModel(
            name='Sayfa_paylasim',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('icerik', models.TextField(max_length=1000)),
            ],
        ),
        migrations.CreateModel(
            name='Sayfalar',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('sayfa_adi', models.CharField(default=None, max_length=250)),
                ('yayinda_mi', models.BooleanField(default=False)),
                ('sayfa_tipi', models.CharField(default='genel', max_length=250)),
            ],
        ),
        migrations.AddField(
            model_name='sayfa_paylasim',
            name='sayfa',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app.Sayfalar'),
        ),
        migrations.AddField(
            model_name='emlak_bilgileri',
            name='ev_kolayliklari',
            field=models.ManyToManyField(to='app.Ev_kolayliklari'),
        ),
        migrations.AddField(
            model_name='emlak_bilgileri',
            name='konut_tipi',
            field=models.ManyToManyField(to='app.Konut_Tipi'),
        ),
        migrations.AddField(
            model_name='emlak_bilgileri',
            name='mulk_kurallari',
            field=models.ManyToManyField(to='app.Mulk_kurallari'),
        ),
        migrations.AddField(
            model_name='emlak_bilgileri',
            name='oda_tipi',
            field=models.ManyToManyField(to='app.Oda_Tipi'),
        ),
        migrations.AddField(
            model_name='emlak_bilgileri',
            name='profil',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='dokumanlar',
            name='galeri',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='images', to='app.Galeri'),
        ),
    ]
