# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
import django

from django.contrib.auth.models import User
import os
import datetime


def image_address(instance,file_name):
    typee = uzanti = os.path.splitext(file_name)[-1]
    filee_save_name = os.path.join('user_image',str(instance.user.id)+typee)
    return filee_save_name

class Profil_info(models.Model):
	user= models.OneToOneField(User)
	image_url = models.ImageField(upload_to = image_address,default= 'documents/no_image.png')
	telefon= models.CharField(max_length=12)
	meslek = models.CharField(max_length = 250)
	okul = models.CharField(max_length = 250)
	dogum_tarihi = models.DateTimeField(default="1980-01-01")
	biyografi=models.TextField(max_length=5000,default=None)
	facebook=models.CharField(max_length = 500, default = None)
	twitter=models.CharField(max_length = 500, default = None)


class Profil_ranked(models.Model):
	sayac    =models.IntegerField(default=0)
	toplam =models.IntegerField(default=0)
	puan = models.IntegerField(default=0)
	profil=models.OneToOneField(Profil_info)
class Oda_Tipi(models.Model):
	oda_tipi_ad=models.CharField(max_length=50)
class Konut_Tipi(models.Model):
	konut_tipi_ad=models.CharField(max_length=50)
class Cinsiyet(models.Model):
	cinsiyet_ad=models.CharField(max_length=50)
class Ev_kolayliklari(models.Model):
	ev_kolayliklari_ad=models.CharField(max_length=50)
class Mulk_kurallari(models.Model):
	mulk_kurallari_ad=models.CharField(max_length=50)
class Banyo_tipi(models.Model):
	banyo_tipi_ad=models.CharField(max_length=50)
class Emlak_bilgileri(models.Model):	
	#emlak_tip = models.CharField(max_length=100) #ozel,paylasimli,tum mekan
	#mekan_tip=  models.CharField(max_length=100) #ev,apartman,yurt,hostel

	profil= models.ForeignKey(User)
	ilan_detayii=models.TextField(max_length=5000,default=None)
	ilan_baslik=models.CharField(max_length=250)
	adres1= models.CharField(max_length=250)
	adres2= models.CharField(max_length=250,default=None)
	ilce=models.CharField(max_length=100)
	sehir=models.CharField(max_length=100)
	posta_kodu=models.CharField(max_length=20,default=None)
	ilan_tarihi=models.DateTimeField()
	website=models.CharField(max_length=100,default=None)

	oda_sayisi=models.IntegerField()
	mutfak_sayisi=models.IntegerField()
	banyo_sayisi=models.IntegerField()
	salon_sayisi=models.IntegerField()
	yatak_oda_sayi= models.IntegerField()
	yatak_sayisi= models.IntegerField()
	genislik=models.IntegerField()
	oda_tipi = models.ManyToManyField(Oda_Tipi) #oda,ortak
	konut_tipi = models.ManyToManyField(Konut_Tipi) #1:kız,2:erkek,3:farketmez
	cinsiyet = models.ManyToManyField(Cinsiyet)
	mulk_kurallari= models.ManyToManyField(Mulk_kurallari)#detay #hayvanlar 
	banyo_tipi = models.ManyToManyField(Banyo_tipi)#yiyecek tipi 
	ev_kolayliklari = models.ManyToManyField(Ev_kolayliklari)
	
	baslangic_tarihi=models.DateTimeField(default=django.utils.timezone.now)
	aylik_ucret=models.FloatField()
	depozito=models.FloatField()
	minumum_kalis=models.IntegerField()	
class Lokasyonlar(models.Model):
	lok_ad=models.CharField(max_length=100)
	lok_x=models.FloatField()
	lok_y=models.FloatField()

def image_address2(instance,file_name):
    typee = uzanti = os.path.splitext(file_name)[-1]
    filee_save_name = os.path.join('image',str(instance.galeri_id)+"_"+str(instance.id)+typee)
    return filee_save_name
class Galeri(models.Model):
	e_bilgileri = models.ForeignKey(Emlak_bilgileri)
	galeri_adi=models.CharField(max_length=250)
	kapak_image=models.ImageField(default='image/kapak/none.jpg',upload_to='image/kapak/')

	def first_image(self):
		# code to determine which image to show. The First in this case.
		return self.images[0]
class Dokumanlar(models.Model):
	galeri=models.ForeignKey(Galeri,related_name='images')
	image_url=models.ImageField(default='image/kapak/none.jpg',upload_to=image_address2)
class Sayfalar(models.Model):
	sayfa_adi=models.CharField(max_length = 250, default = None)
	yayinda_mi=models.BooleanField(default=False)
	sayfa_tipi=models.CharField(max_length=250,default="genel")
class Sayfa_paylasim(models.Model):
	sayfa=models.ForeignKey(Sayfalar)
	icerik=models.TextField(max_length=1000) 



		
		
	
		
		